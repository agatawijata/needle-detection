# Unbiased validation of the algorithms for automatic needle localization in ultrasound-guided breast biopsies


**Agata M. Wijata and Jakub Nalepa**

(Submitted to ICIP 2022)

This repository contains the Supplementary Material which gathers the detailed analysis of the state of the art in the automated biopsy needle localization and tracking algorithms, together with additional results of our experimental study, in which we investigated the impact of various training-test dataset splitting strategies on the overall performance of such techniques.
